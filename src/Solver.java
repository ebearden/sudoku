/**
 * User: Elvin Bearden
 * Date: 1/1/13
 * Time: 11:59 AM
 */


public class Solver {
    private Board board;

    public Solver(Board board){
        this.board = board;
    }

    public void solve(int row, int col) throws Exception {
        Candidate candidate;
        // Throw an exception if board is solved.
        if (row > 8){
            throw new Exception("Solution Found!");
        }

        if (board.numberAt(row, col) < 0){
            next(row, col);
        }
        else{
            for (int i = 1; i < 10; i++){
                candidate = new Candidate(row,col,i);
                if (isValidMove(candidate)){
                    board.setBoard(candidate.getRow(), candidate.getCol(), candidate.getValue());
                    next(row, col);
                }

            }
            board.setBoard(row, col, 0);
        }


    }

    private void next(int row, int col)throws Exception{
        if (col < 8){
            solve(row, col + 1);
        }
        else{
            solve(row + 1, 0);
        }
    }

//    public void Solve(){
//
//        Candidate candidate = new Candidate(0,0,5);
//
//        while(!board.isCompleted()){
//            for (int i = 0; i < 9; i++){
//                for (int j = 0; j < 9; j++){
//                    for (int k = 1; k < 10; k++){
//                        candidate.setRow(i);
//                        candidate.setCol(j);
//                        candidate.setValue(k);
//
//                        if (isValidMove(candidate)){
//                            board.setBoard(candidate.getRow(), candidate.getCol(), candidate.getValue());
//                            board.printBoard();
//                        }
//                    }
//                }
//            }
//        }
//    }

    public boolean isValidMove(Candidate candidate){
        /**
         * Checks to make sure the proposed move is valid.
        */

        // 1. Move is not a given.
        if (candidate.getValue() > 0){
            return false;
        }

        // 2. Row does not contain the number
        for (int columnIndex = 0; columnIndex < 9; columnIndex++){
                if (candidate.getValue() == board.numberAt(candidate.getRow(), columnIndex)){
                    return false;
                }
            }

        // 3. Column does not contain the number
        for (int rowIndex = 0; rowIndex < 9; rowIndex++){
            if (candidate.getValue() == board.numberAt(rowIndex, candidate.getCol())){
                return false;
            }
        }

        // 4. 3 x 3 does not contain the number
        switch (determineSector(candidate)){
            case 1:
                for (int i = 0; i < 3; i++){
                    for (int j = 0; j < 3; j++){
                        if (candidate.getValue() == board.numberAt(i, j))
                        {
                            return false;
                        }
                    }
                }
                break;
            case 2:
                for (int i = 0; i < 3; i++){
                    for (int j = 3; j < 6; j++){
                        if (candidate.getValue() == board.numberAt(i, j))
                        {
                            return false;
                        }
                    }
                }
                break;
            case 3:
                for (int i = 0; i < 3; i++){
                    for (int j = 6; j < 9; j++){
                        if (candidate.getValue() == board.numberAt(i, j))
                        {
                            return false;
                        }
                    }
                }
                break;
            case 4:
                for (int i = 3; i < 6; i++){
                    for (int j = 0; j < 3; j++){
                        if (candidate.getValue() == board.numberAt(i, j))
                        {
                            return false;
                        }
                    }
                }
                break;
            case 5:
                for (int i = 3; i < 6; i++){
                    for (int j = 3; j < 6; j++){
                        if (candidate.getValue() == board.numberAt(i, j))
                        {
                            return false;
                        }
                    }
                }
                break;
            case 6:
                for (int i = 3; i < 6; i++){
                    for (int j = 6; j < 9; j++){
                        if (candidate.getValue() == board.numberAt(i, j))
                        {
                            return false;
                        }
                    }
                }
                break;
            case 7:
                for (int i = 6; i < 9; i++){
                    for (int j = 0; j < 3; j++){
                        if (candidate.getValue() == board.numberAt(i, j))
                        {
                            return false;
                        }
                    }
                }
                break;
            case 8:
                for (int i = 6; i < 9; i++){
                    for (int j = 3; j < 6; j++){
                        if (candidate.getValue() == board.numberAt(i, j))
                        {
                            return false;
                        }
                    }
                }
                break;
            case 9:
                for (int i = 6; i < 9; i++){
                    for (int j = 6; j < 9; j++){
                        if (candidate.getValue() == board.numberAt(i, j))
                        {
                            return false;
                        }
                    }
                }
                break;
            default:
                System.out.println("Error in isValidMove()");

        }


        return true;
    }

    private int determineSector(Candidate candidate){

        // Determine row sector
        int rowSector;

        if (candidate.getRow() >= 0 && candidate.getRow() < 3){
            rowSector = 1;
        }
        else if (candidate.getRow() > 2 && candidate.getRow() < 6){
            rowSector = 2;
        }
        else{
            rowSector = 3;
        }

        // Determine col sector
        int colSector;

        if (candidate.getCol() >= 0 && candidate.getCol() < 3){
            colSector = 1;
        }
        else if (candidate.getCol() > 2 && candidate.getCol() < 6){
            colSector = 2;
        }
        else{
            colSector = 3;
        }

        // Determine final sector
        int finalSector;
        if (rowSector == 1){
            if(colSector == 1){
                finalSector = 1;
            }
            else if(colSector == 2){
                finalSector = 2;
            }
            else{
                finalSector = 3;
            }
        }
        else if (rowSector == 2){
            if(colSector == 1){
                finalSector = 4;
            }
            else if(colSector == 2){
                finalSector = 5;
            }
            else{
                finalSector = 6;
            }
        }
        else{
            if (colSector == 1){
                finalSector = 7;
            }
            else if (colSector == 2){
                finalSector = 8;
            }
            else{
                finalSector = 9;
            }
        }

        return finalSector;
    }


}
