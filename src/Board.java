/**
 * User: Elvin Bearden
 * Date: 12/31/12
 * Time: 9:43 PM
 */

import java.io.*;
import java.util.Scanner;

public class Board {
    private int[][] board = new int[9][9];
    private String filename;
    private Scanner inStream;

    /**
     * Creates a new sudoku board object.
     *
     * @param filename
     * The file containing the sudoku board information.
     */
    public Board(String filename){
        this.filename = filename;
        populateArray();
    }

    /**
     * Prints the board to the console.
     */
    public void printBoard(){
        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board.length; j++){
                System.out.print(Math.abs(board[i][j]) + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Get the value from the board
     *
     * @param row
     * @param col
     * @return
     * value at (row, col)
     */
    public int numberAt(int row, int col){
        return board[row][col];
    }

    /**
     * Set the space at (row, col) to the specified value.
     *
     * @param row
     * @param col
     * @param value
     */
    public void setBoard(int row, int col, int value){
        board[row][col] = value;
    }

    /**
     * Loads Sudoku board from txt file and populates array.
     */
    private void populateArray(){

        // Load file
        try{
            inStream = new Scanner(new FileReader(filename));
        }
        catch (FileNotFoundException ex){
            System.out.println("File Not Found!");
        }

        // Read file
        StringBuilder strBuilder = new StringBuilder();

        while (inStream.hasNext()){
            strBuilder.append(inStream.next());
        }

        char c[] = strBuilder.toString().toCharArray();

        // Populate array
        int index = 0;

        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board.length; j++){
                board[i][j] = c[index] - 48;
                board[i][j] *= -1;
                index++;
            }
        }
    }

    /**
     * Check if board is completed.
     *
     * @return
     * True if board has no empty spaces.
     */
    public boolean isCompleted(){

        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board.length; j++){
                if (board[i][j] == 0){
                    return false;
                }
            }
        }

        return true;
    }

}

