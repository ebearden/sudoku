/**
 * User: Elvin Bearden
 * Date: 1/1/13
 * Time: 3:58 PM
 */
public class Candidate {
    private int row, col, value;

    public Candidate(int row, int col, int value){
        this.row = row;
        this.col = col;
        this.value = value * -1;
    }

    public int getRow(){
        return row;
    }

    public void setRow(int n){
        row = n;
    }

    public int getCol(){
        return col;
    }

    public void setCol(int n){
        col = n;
    }

    public int getValue(){
        return value;
    }

    public void setValue(int n){
        value = n * -1;
    }
}
