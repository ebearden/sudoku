/**
 * User: Elvin Bearden
 * Date: 12/31/12
 * Time: 9:43 PM
 */

public class Sudoku {
    public static void main(String[] args){
        Board b1 = new Board("gridOne.txt");
        b1.printBoard();

        Solver solver = new Solver(b1);
        try{
        solver.solve(0,0);
        }
        catch (Exception ex){
            b1.printBoard();
        }

        Board b2 = new Board("gridTwo.txt");
        b2.printBoard();
        solver = new Solver(b2);

        try{
            solver.solve(0,0);
        }
        catch (Exception ex){
            b2.printBoard();
        }

        Board b3 = new Board("gridThree.txt");
        b3.printBoard();
        solver = new Solver(b3);

        try{
            solver.solve(0,0);
        }
        catch (Exception ex){
            b3.printBoard();
        }

    }
}
